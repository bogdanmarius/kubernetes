Welcome to the Kubernetes repository

The files hosted here serve and an example for learning how to use kubectl without breaking production.

Before you start:
- install docker since this is a requirement of minikube
https://gitlab.com/bogdanmarius/commands/-/blob/master/my-docker-commands.txt?ref_type=heads
- install minikube to have a cluster emulation of kubernetes locally
https://gitlab.com/bogdanmarius/commands/-/blob/master/my-minikube-K8s-commands.txt?ref_type=heads
- to start the journey here is a small cheat sheet
https://gitlab.com/bogdanmarius/commands/-/blob/master/my-K8s-commands.txt?ref_type=heads

For more details and examples of course you can use
https://kubernetes.io/docs/reference/kubectl/cheatsheet/

Check minikube status: 
-> minikube status

Check kubectl version and execution:
-> kubectl version

Execution of yaml files:
- to start a pod/deployment use: kubectl apply -f name-of-file.yml
- to stop a pod/deployment use: kubectl delete -f name-of-file.yml